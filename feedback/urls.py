"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include

from . import views

urlpatterns = [
    path("database/", include("feedback.database.urls")),
    path("courses/", views.get_courses_list, name="courses"),
    path("courses/add/", views.add_course, name="add_course"),
    # FIXME
    path("courses/<int:course_id>/", views.get_course, name="get_course"),
    path(
        "courses/<int:course_id>/polls/",
        views.get_polls_by_course,
        name="get_polls_by_course",
    ),
    # polls
    path(
        "courses/<int:course_id>/polls/",
        views.get_polls_by_course,
        name="get_polls_list",
    ),
    path(
        "courses/<int:course_id>/polls/<int:poll_id>/",
        views.get_poll,
        name="get_poll",
    ),
    path(
        "courses/<int:course_id>/polls/add/", views.add_poll, name="add_poll"
    ),
    path(
        "courses/<int:course_id>/polls/<int:poll_id>/answer/",
        views.add_feedback,
        name="leave_feedback",
    ),
    # feedback view
    path(
        "courses/<int:course_id>/feedback/",
        views.get_feedback_by_course,
        name="course_feedback",
    ),
    path(
        "courses/<int:course_id>/feedback/<feedback_id>/",
        views.get_feedback,
        name="particular feedback",
    ),
    path(
        "courses/<int:course_id>/feedback_by_form/<form_id>/",
        views.get_feedback_by_form,
        name="feedback of a form",
    ),
    # auth
    path("users/register/", views.register_user),
    path("users/type/", views.get_user_type),
]

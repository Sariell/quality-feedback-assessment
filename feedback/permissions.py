from rest_framework.permissions import BasePermission


class ReadPermission(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.has_perm("feedback.can_read_course"))


class AddPollPermission(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.has_perm("feedback.can_add_poll"))


class AddCoursePermission(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.has_perm("feedback.can_add_course"))


class AddFeedbackPermission(BasePermission):
    def has_permission(self, request, view):
        return bool(request.user.has_perm("feedback.can_add_feedback"))

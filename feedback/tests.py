import json

from django.contrib.auth.models import Permission
from django.test import TestCase

APPLICATION_JSON = "application/json"

USERNAME = "a@a.a"

CREDENTIALS = (
    """
    {
     "username": "%s",
     "password": "testuser"
    }
"""
    % USERNAME
)


class TestAuth(TestCase):
    def test_registration(self):
        from django.contrib.auth.models import User

        self.client.post(
            "/users/register/", data=CREDENTIALS, content_type=APPLICATION_JSON
        )

        users = User.objects.all()
        contains = False
        for user in users:
            if user.username == USERNAME:
                contains = True
        self.assertTrue(contains)

    def test_obtain_token(self):
        self.test_registration()

        r = self.client.post(
            "/auth/obtain_token/",
            data=CREDENTIALS,
            content_type="application/json",
        )
        token = json.loads(r.content)["token"]

        self.assertIsNotNone(token)

    def test_obtain_token_wrong_user(self):
        self.test_registration()

        creds = """
            {
             "username": "nota@a.a",
             "password": "testuser"
            }
        """
        r = self.client.post(
            "/auth/obtain_token/", data=creds, content_type="application/json"
        )
        self.assertEquals(r.status_code, 400)

    def test_permissions(self):
        from django.contrib.auth.models import User

        self.test_registration()

        r = self.client.post(
            "/auth/obtain_token/",
            data=CREDENTIALS,
            content_type="application/json",
        )
        token = json.loads(r.content)["token"]

        users = User.objects.all()
        current_user = None
        for user in users:
            if user.username == USERNAME:
                current_user = user

        permission = Permission.objects.all().get(name="Can read the course")
        current_user.user_permissions.add(permission)

        header = {"HTTP_AUTHORIZATION": "Bearer " + token}
        r = self.client.get("/courses/", **header)
        self.assertEquals(r.status_code, 200)
        courses = json.loads(r.content)
        self.assertGreaterEqual(len(courses), 1)

    def test_negative_permissions(self):
        from django.contrib.auth.models import User

        self.test_registration()

        r = self.client.post(
            "/auth/obtain_token/",
            data=CREDENTIALS,
            content_type="application/json",
        )
        token = json.loads(r.content)["token"]

        users = User.objects.all()
        current_user = None
        for user in users:
            if user.username == USERNAME:
                current_user = user

        header = {"HTTP_AUTHORIZATION": "Bearer " + token}
        r = self.client.get("/courses/", **header)
        self.assertEquals(r.status_code, 403)
        answer = json.loads(r.content)
        self.assertIsNotNone(answer.get("detail"))
        message = answer["detail"]
        self.assertEquals(
            message, "You do not have permission to perform this action."
        )

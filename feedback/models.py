import datetime

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class Course(models.Model):
    class Meta:
        permissions = (
            ("can_read_course", "Can read the course"),
            ("can_add_course", "Can add a course"),
        )


class Poll(models.Model):
    class Meta:
        permissions = (("can_add_poll", "Can add a poll"),)


class Feedback(models.Model):
    class Meta:
        permissions = (("can_add_feedback", "Can add feedback"),)

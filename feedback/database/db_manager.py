import json

from arango import ArangoClient
from arango_orm import Collection
from arango_orm import Database
from arango_orm.fields import String, Raw


# allows to transform a arango-orm query into a nice-formatted JSON.
def query_to_json(query):
    out = []
    for i in query:
        out.append(i.__dict__())
    return json.dumps(out)


# same method for single output (not a list of JSON, but only one)
def query_to_json_single(query):
    out = []
    for i in query:
        out.append(i.__dict__())
    return json.dumps(out[0])


# Database manager class. Works with the ArangoDB via Arango-orm library.
class DatabaseManager:
    def get_courses_list(self):  # return JSON
        query = self.db.query(Course).all()
        return query_to_json(query)

    def get_course(self, course_id):
        query = self.db.query(Course).filter_by(_key=str(course_id)).all()
        return query_to_json_single(query)

    def add_course(self, body):
        body = json.loads(body)
        course = Course(
            name=body["name"],
            prof=body["prof"],
            ta=body["ta"],
            img=body["img"],
            date_from=body["date_from"],
            date_to=body["date_to"],
        )
        self.db.add(course)
        return

    def get_polls(self):
        query = self.db.query(Form).all()
        return query_to_json(query)

    def get_poll(self, poll_id):
        query = self.db.query(Form).filter_by(_key=str(poll_id)).all()
        return query_to_json_single(query)

    def add_poll(self, body):
        print(body)
        body = json.loads(body)
        form = Form(
            course=body["course"],
            type=body["type"],
            week=body["week"],
            topic=body["topic"],
            availability=body["availability"],
            link=body["link"],
            questions=body["questions"],
        )
        self.db.add(form)
        return

    def add_feedback(self, body, course_id):
        body = json.loads(body)
        feedback = Feedback(
            form_id=body["key"], answers=body["answers"], course=course_id
        )
        self.db.add(feedback)
        return

    def get_polls_by_course(self, course_id):
        query = self.db.query(Form).filter_by(course=str(course_id)).all()
        return query_to_json(query)

    def get_feedback(self, feedback_id):
        # finding form id needed
        query_form_id = (
            self.db.query(Feedback).filter_by(_key=str(feedback_id)).all()
        )
        query_form_id = query_to_json(query_form_id)
        query_form_id = json.loads(query_form_id)
        form_id = query_form_id[0]["form_id"]
        print("form id = " + str(form_id) + "\n")

        # finding questions for corresponding form id
        query_questions = (
            self.db.query(Form).filter_by(_key=str(form_id)).all()
        )
        query_questions = json.loads(query_to_json(query_questions))[0][
            "questions"
        ]

        # appending questions to answers
        query_answers = (
            self.db.query(Feedback).filter_by(_key=str(feedback_id)).all()
        )
        query_answers = query_answers[0].__dict__()
        query_answers["questions"] = query_questions

        return json.dumps(query_answers)

    def get_feedback_by_course(self, course_id):
        query = self.db.query(Feedback).filter_by(course=str(course_id)).all()
        return query_to_json(query)

    def get_feedback_by_form(self, form_id):
        query = self.db.query(Feedback).filter_by(form_id=str(form_id)).all()
        return query_to_json(query)

    def __init__(self):
        self.client = ArangoClient(
            protocol="http", host="54.93.180.11", port=8529
        )
        self.database = self.client.db(
            "feedback", username="team", password="quality"
        )
        self.db = Database(self.database)


class Course(Collection):
    __collection__ = "course"
    # возможно fields надо просто '_key'
    _index = [
        {
            "type": "primary",
            "unique": True,
            "fields": ["name", "prof", "ta", "img", "date_from", "date_to"],
        }
    ]
    _key = String(required=True)
    name = String(required=True, allow_none=False)
    prof = String(required=True)
    ta = String(required=True)
    img = String(required=False)
    date_from = String(required=True)
    date_to = String(required=True)

    def __dict__(self):
        dic = {
            "_key": str(self._key),
            "_id": str(self._id),
            "name": str(self.name),
            "prof": str(self.prof),
            "ta": str(self.ta),
            "img": str(self.img),
            "date_from": str(self.date_from),
            "date_to": str(self.date_to),
        }
        return dic


class Form(Collection):
    __collection__ = "form"
    _index = [
        {
            "type": "primary",
            "unique": True,
            "fields": [
                "course",
                "type",
                "week",
                "topic",
                "availablitiy",
                "link",
                "questions",
            ],
        }
    ]
    _key = String(required=True)
    course = String(required=True)
    week = String(required=True)
    topic = String(required=True)
    availability = String(required=True)
    link = String(required=False)
    # возможно List
    questions = Raw(required=True)

    def __dict__(self):
        dic = {
            "_key": str(self._key),
            "_id": str(self._id),
            "course": str(self.course),
            "week": str(self.week),
            "topic": str(self.topic),
            "availability": str(self.availability),
            "link": str(self.link),
            "questions": self.questions,
        }
        return dic


class Feedback(Collection):
    __collection__ = "feedback"
    _index = [
        {
            "type": "primary",
            "unique": True,
            "fields": ["course", "student_id", "form_id", "answers"],
        }
    ]
    _key = String(required=True)
    course = String(required=True)
    student_id = String(required=False, allow_none=True)
    form_id = String(required=True)
    answers = Raw(required=True)

    def __dict__(self):
        dic = {
            "_key": str(self._key),
            "_id": str(self._id),
            "course": str(self.course),
            "student_id": str(self.student_id),
            "form_id": str(self.form_id),
            "answers": self.answers,
        }
        return dic

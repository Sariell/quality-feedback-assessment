from django.http import HttpResponse

from feedback.database import db_manager


def receive(request):
    return HttpResponse(db_manager.receive())


def send(request):
    return HttpResponse(db_manager.send(request))

from django.http import HttpResponse
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import AllowAny

from feedback import auth_service
from feedback.database.db_manager import DatabaseManager
from feedback.permissions import (
    ReadPermission,
    AddPollPermission,
    AddCoursePermission,
    AddFeedbackPermission,
)

db_manager = DatabaseManager()


@api_view(["GET"])
@permission_classes([ReadPermission])
def get_courses_list(request):
    """
    Gets the list of courses.
    """
    return HttpResponse(db_manager.get_courses_list())


@api_view(["GET"])
@permission_classes([ReadPermission])
def get_course(request, course_id):
    """
    Gets the specific course.
    """
    return HttpResponse(db_manager.get_course(course_id))


@api_view(["GET"])
@permission_classes([ReadPermission])
def get_poll(request, course_id, poll_id):
    """
    Gets the specific poll.
    """
    return HttpResponse(db_manager.get_poll(poll_id))


@api_view(["GET"])
@permission_classes([ReadPermission])
def get_polls(request):
    """
    Gets list of all polls.
    """
    return HttpResponse(db_manager.get_polls())


@api_view(["GET"])
@permission_classes([ReadPermission])
def get_polls_by_course(request, course_id):
    """
    Gets polls by course ID.
    """
    return HttpResponse(db_manager.get_polls_by_course(course_id))


@api_view(["POST"])
@permission_classes([AddPollPermission])
def add_poll(request, course_id):
    """
    Adds a new poll.
    ---
    By default professors and administrators can do that.
    """
    return HttpResponse(db_manager.add_poll(request.body))


@api_view(["POST"])
@permission_classes([AddCoursePermission])
def add_course(request):
    """
    Adds a new course.
    ---
    By default only administrators can do that.
    """
    return HttpResponse(db_manager.add_course(request.body))


@api_view(["POST"])
@permission_classes([AddFeedbackPermission])
def add_feedback(request, course_id, poll_id):
    """
    Adds feedback from the user.
    """
    return HttpResponse(db_manager.add_feedback(request.body, course_id))


def get_feedback(request, course_id, feedback_id):
    """
    Gets feedback for the specific course.
    """
    return HttpResponse(db_manager.get_feedback(feedback_id))


@api_view(["GET"])
@permission_classes(
    [AddCoursePermission]
)  # сделать пермишн на просмотр фидбека курса (AddCoursePermission
def get_feedback_by_course(
    request, course_id
):  # подходит, но лучше либо переименовать, либо сделать отдельный)
    return HttpResponse(db_manager.get_feedback_by_course(course_id))


@api_view(["POST"])
@permission_classes([AllowAny])
def register_user(request):
    """
    Registers the user in the app.
    ---
    By default user is registered with no user group.
    """
    return HttpResponse(auth_service.register(request.body))


@api_view(["GET"])
@permission_classes([AllowAny])
def get_user_type(request):
    """
    Returns the type of user.
    ---
    Auth token must be passed in headers.
    """
    return HttpResponse(auth_service.get_type(request))


@api_view(["GET"])
@permission_classes([AddCoursePermission])
def get_feedback_by_form(request, course_id, form_id):
    return HttpResponse(db_manager.get_feedback_by_form(form_id))

# Quality Feedback

## Run
If you run the project for the first time, you should firstly prepare your python.  
1. Install `pipenv` by typing
	```bash
	pip install pipenv
	```
1. Install all the dependencies for the project
	```bash
	pipenv install
	```
1. Run the server
	```bash
	pipenv run ./manage.py runserver
	```
1. Connect to the running server at localhost:8000

## Pre-commit

The project has code formatter. If you want it to work, you should execute after `pipenv install`:
```bash
    pipenv run pre-commit install
```